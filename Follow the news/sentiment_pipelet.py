#!/usr/bin/python

import requests
from squirro.sdk import PipeletV1
import json

class NoopPipelet(PipeletV1):
    def consume(self, item):
        body = item.get('abstract',' ')
        
        headers = {
            "X-AYLIEN-TextAPI-Application-Key": "4e6a8017e90da59d9ba842a3e6aa7e81",
            "X-AYLIEN-TextAPI-Application-ID": "65cba0ef" 
        }
        data = {
            'mode': "tweet",
            'text': "body"
        }
        response = requests.post("https://api.aylien.com/api/v1/sentiment", headers=headers, data=data)
        resp_dict=json.loads(response.content)
        
        #print (type(response.content))
        
        item.get('keywords',{})['sentiment'] = [resp_dict.get('polarity', ' ')]

        return item